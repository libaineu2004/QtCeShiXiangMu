﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_btnFile_clicked();

    void on_btnLetter_clicked();

    void on_btnSymbol_clicked();

    void on_btnValue_clicked();

    void on_btnEmptyLine_clicked();

    void on_btnZero_clicked();

private:
    Ui::Widget *ui;
    QStringList data;
};
#endif // WIDGET_H
