﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include "jgraphicsview.h"
#include "jgraphicstextitem.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
class QGraphicsScene;
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
protected slots:
    void btnOPen_clicked();
private:
    Ui::Widget *ui;
    QGraphicsScene *scene;
    JGraphicsView *View;
    QPushButton *btnOpen;
    QHBoxLayout *hLayMain;
};
#endif // WIDGET_H
