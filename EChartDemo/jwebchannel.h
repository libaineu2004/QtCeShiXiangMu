﻿#ifndef JWEBCHANNEL_H
#define JWEBCHANNEL_H

#include <QObject>
#include <QWebEnginePage>

class JWebChannel : public QObject
{
    Q_OBJECT
public:
    explicit JWebChannel(QObject *parent = nullptr);
    // 向页面发送消息
    void SendMsg(QWebEnginePage* page, const QString &msg);
signals:
    void RecvMsg(const QString &msg);
public slots:
    // 接受页面发送的信息
    void RecvJSMsg(const QString &msg);
};

#endif // JWEBCHANNEL_H
